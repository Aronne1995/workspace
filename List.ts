import {Color} from './Color.enum';
import {Reminder} from './Reminder';

export class List {
  id: string;
  name: string;
  priority: number;
  color: Color;
  reminders: Reminder[];

  constructor(
    name: string,
    priority: number,
    color: Color = Color.Blu,
    reminders: Reminder[] = [],
    id: string = parseInt(Math.random() * 10000 + '', 10) + '',
  ) {
    this.id = id;
    this.name = name;
    this.priority = priority;
    this.color = color;
    this.reminders = reminders;
    this.reminders.push(new Reminder({order: 0, name: "Test"}));
  }
}
