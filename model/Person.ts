export class Person {
  name: string;
  surname: string;
  // MODIFICATORI DI ACCESSO:
  // public: visibile a tutti, posso anche non scriverlo
  // private: visibile solo nella classe stessa tramite this.age
  // protected: visibile solo nella classe stessa e in quella che la estende tramite this.age
  protected age: number;
  
  constructor(name: string, surname: string = "Rossi", personAge: number = 0) {
    this.name = name;
    this.surname = surname;
    this.age = personAge;
  }
  
  getFullName(): string {
    return this.name + ' ' + this.surname;
  }
  
  getAge(): number {
    console.log('Age:', this.age);
    return this.age - 10;
  }
}
