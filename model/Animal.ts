export interface Animal {
	// Attributi (caratteristiche)
	name?: string;  // opzionale perchè c'è ?, pertanto la classe potrebbe non avere quell'attributo
	race?: string;
	legs: number;
	color: string;
	eyes: any[];

	// Metodi (azioni)
	eat: Function;
	breathe: Function;
	drink?: Function;
	walk?: Function;
}
