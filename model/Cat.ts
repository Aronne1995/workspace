import {Animal} from "./Animal";

export class Cat implements Animal {
	legs: number = 4;
	color: string;
	eyes: any[];

	constructor(catColor: string, catEyes: any[]) {
		this.color = catColor; // this faccio riferimento al gatto che sto creando, non a tutti i gatti
		this.eyes = catEyes;
	}

	eat(food: string): boolean {
		console.log(food);
		return true;
	}

	breathe(): void {

	}
}
