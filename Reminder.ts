interface IReminder {
  name: string;
  order: number;
  description?: string;
  done?: boolean;
}

export class Reminder implements IReminder {
  name: string;
  order: number;
  description?: string;
  done?: boolean;

  constructor(obj: IReminder) {
    this.name = obj.name;
    this.order = obj.order;
    this.description = obj.description;
    this.done = obj.done;
  }
}
